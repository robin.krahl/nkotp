=head1 NAME

nkotp - one-time password generator for Nitrokey devices

=head1 SYNOPSIS

B<nkotp>
S<[B<-a> I<algorithm>]>
S<[B<-c> I<file>]>
S<[B<-m> I<model>]>
S<[B<-s> I<slot>]>
S<B<-g> | B<-h> | B<-v>>

=head1 DESCRIPTION

B<nkotp> provides access to the one-time password (OTP) generator on Nitrokey
devices.  Currently, B<nkotp> only supports the generation of OTPs.

If an action requires the user password, it is prompted from the standard
input or read from the environment variable B<NKOTP_USER_PASSWORD> (if set).

=head1 OPTIONS

=head2 General options

=over

=item B<-a> I<algorithm>, B<--algorithm> I<algorithm>

Set the algorithm to use for one-time password operations.  I<algorithm> can be
B<h> for HOTP and B<t> for TOTP (default).

=item B<-c> I<file>, B<--config> I<file>

Read the configuration from I<file>.  See the B<FILES> section for the default
configuration files.

=item B<-m> I<model>, B<--model> I<model>

Set the Nitrokey model to connect to.  I<model> can be B<p> for a Nitrokey Pro,
B<s> for a Nitrokey Storage and B<a> for automatic selection (default).

=item B<-s> I<slot>, B<--slot> I<slot>

Set the slot to use for one-time password operations.  The available slots
depend on the OTP algorithm (see B<--algorithm>).  Currently, Nitrokey devices
provide three HOTP and 15 TOTP slots.  The slot numbering starts at one.  The
default value for this option is one.

=back

=head2 Modes of operation

=over

=item B<-g>, B<--generate>

Generate a one-time password on the Nitrokey device and output it.  The OTP
algorithm is set with the B<--algorithm> option.  The OTP slot on the Nitrokey
device is set with the B<--slot> option.

=item B<-h>, B<--help>

Print a help message and exit.

=item B<-v>, B<--version>

Print version information and exit.

=back

=head1 CONFIGURATION

B<nkotp> can read default values for the command-line options from a
configuration file.  See the B<FILES> section for more information on the
possible locations for the configuration file.

The configuration file may assign values to the following options:

=over

=item B<algorithm>

=item B<device>

=item B<slot>

=back

Each option corresponds to the command-line option with the same name.  Values
set in the configuration file take precedence over environment variables.

The configuration file should contain one assignment per line.  Assignments
have the form C<option = value>.  String values must be enclosed in quotes.
Use the C<#> character for comments.

A valid configuration file could have the following content:

	# configuration example
	algorithm = "t"
	slot = 3

=head1 ENVIRONMENT

=over

=item B<NKOTP_ALGORITHM>

=item B<NKOTP_CONFIG>

=item B<NKOTP_DEVICE>

=item B<NKOTP_SLOT>

If these environment variables are set, they override the default value for
the corresponding command-line option.  Values that are set in the
configuration file take precedence over environment variables.

=item B<NKOTP_USER_PASSWORD>

If an action requires the user password, it is read from this environment
variable (if set).

=back

=head1 FILES

=over

=item B<${XDG_CONFIG_HOME}/nkotp/config>

User configuration file.  If the environment variable B<XDG_CONFIG_HOME> is
not set, B<${HOME}/.config> is used instead.  A different configuration file
can be set with the B<NKOTP_CONFIG> environment variable or the B<--config>
option.

=back

=head1 AUTHOR

Robin Krahl E<lt>robin.krahl@ireas.orgE<gt>
