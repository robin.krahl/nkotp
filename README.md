# nkotp -- one-time password generator for Nitrokey devices on the command line

`nkotp` provides access to the one-time password (OTP) generator on
[Nitrokey][nk] devices on the command line.  Both the Nitrokey Pro and the
Nitrokey Storage support the generation of one-time passwords based on the
[HOTP][hotp] and [TOTP][totp] algorithms.  `nkotp` uses [`libnitrokey`][libnk]
to configure the OTP slots and generate OTPs on a Nitrokey device.

## Dependencies

`nkotp` requires a POSIX-compliant operating system such as Linux or macOS.

### Runtime dependencies

- `libc` with `getopt_long` support
- [`libconfuse`][libconfuse] v3.0.0 or later
- [`libnitrokey`][libnk] v3.0 or later

### Additional build dependencies

- `gcc` or any other C99 compiler
- GNU `make`
- `pod2html`, `pod2man` (usually distributed with `perl`)

## Compilation

Build `nkotp` with `make`.  You can configure the build in `config.mk` if you
have non-standard library paths or compiler flags.

## Usage

For usage information, consult the man page [`nkotp(1)`][man].

## Bugs and hacking

If you encouter a bug or if you want to contribute to nkotp, please send an
email to [nkotp-dev@ireas.org][nkotp-dev].

## License

This program is published under the terms of the MIT/X11 license (see LICENSE).

[nk]: https://www.nitrokey.com/
[hotp]: https://en.wikipedia.org/wiki/HMAC-based_One-time_Password_Algorithm
[totp]: https://en.wikipedia.org/wiki/Time-based_One-time_Password_Algorithm
[libconfuse]: https://github.com/martinh/libconfuse
[libnk]: https://github.com/Nitrokey/libnitrokey
[man]: https://code.ireas.org/nkotp/doc/
[nkotp-dev]: mailto:nkotp-dev@ireas.org
