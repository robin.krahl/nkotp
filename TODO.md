- Decide what we want to do if passwords are too long:  truncate, fail
  silently, fail with error message (current solution).  Affects
  `read_user_password` and `read_password` in `nkotp.c`.
- Consider moving some constants to `libnitrokey`, especially the maximum
  password lengths and the number of HOTP and TOTP slots.
- Find out the appropriate values to pass to `NK_get_totp_code` and
  `NK_get_totp_code_PIN`.  Affects `otp_generate` and `otp_generate_password`
  in `nkotp.c`.
- Use a better seed than the current time stamp for the generation of the
  temporary password (`generate_tmp_password` in `nkotp.c`).  Consider
  switching to a better random number generator in the first place
  (`getrandom` for Linux).
