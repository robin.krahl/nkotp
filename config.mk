# Version information
VERSION_MAJOR := 0
VERSION_MINOR := 0
VERSION_PATCH := 1

# Dependencies
CPPFLAGS_CONFUSE :=
CPPFLAGS_NITROKEY :=
LDFLAGS_CONFUSE := -lconfuse
LDFLAGS_NITROKEY := -lnitrokey

# Compiler flags
CFLAGS += -std=c99 -pedantic -Wall
CPPFLAGS += -D_XOPEN_SOURCE=700 -D_GNU_SOURCE

# Man page generation
P2MFLAGS += --section=1 --center=nkotp --name=NKOTP
P2HFLAGS += --noindex --title="nkotp(1)"
