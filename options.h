/*
 * Copyright (c) 2018 Robin Krahl <robin.krahl@ireas.org>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef NKOTP_OPTIONS_H
#define NKOTP_OPTIONS_H

#include <stddef.h>

enum mode {
	MODE_NONE,
	MODE_GENERATE
};

enum algorithm {
	ALGORITHM_DEFAULT,
	ALGORITHM_HOTP,
	ALGORITHM_TOTP
};

enum model {
	MODEL_DEFAULT,
	MODEL_AUTO,
	MODEL_PRO,
	MODEL_STORAGE
};

struct options {
	enum algorithm alg;
	char *cfg_file;
	char model;
	int slot;
	enum mode mode;
};

static const size_t MAX_SLOT_HOTP = 3;
static const size_t MAX_SLOT_TOTP = 15;

int parse_options(int argc, char **argv, struct options *options);
void free_options(struct options *options);

#endif
