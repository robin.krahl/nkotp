include config.mk

VERSION := $(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_PATCH)

CPPFLAGS += -DNKOTP_VERSION_MAJOR=$(VERSION_MAJOR) \
	    -DNKOTP_VERSION_MINOR=$(VERSION_MINOR) \
	    -DNKOTP_VERSION_PATCH=$(VERSION_PATCH) \
	    -DNKOTP_VERSION=\"$(VERSION)\"
CPPFLAGS += $(CPPFLAGS_CONFUSE)
CPPFLAGS += $(CPPFLAGS_NITROKEY)
LDFLAGS += $(LDFLAGS_CONFUSE)
LDFLAGS += $(LDFLAGS_NITROKEY)

P2MFLAGS += --release=$(VERSION)

OBJECTS := nkotp.o options.o
TARGETS := nkotp nkotp.1 nkotp.1.html

.PHONY: all clean

all: $(TARGETS)

clean:
	rm -f $(OBJECTS) $(TARGETS) pod2htmd.tmp

nkotp: $(OBJECTS)

nkotp.1: nkotp.1.pod
	pod2man $(P2MFLAGS) $^ > $@

nkotp.1.html: nkotp.1.pod
	pod2html $(P2HFLAGS) $^ > $@
